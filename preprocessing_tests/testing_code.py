import numpy as np
import pandas as pd

import sys
sys.path.append('../')
from lib.Dab import Dab, get_linear_weights
from lib.Gmt import read_gmt

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

import scipy.stats
import igraph

from lib.clustering import *


### TESTING INTERNAL CONNECTIVITY

def internal_connectivity_distribution(dab, set_size, samples=2500):
    """Simulates the distribution of internal connectivity for sets of a given size
       in the dab network"""
    results = []
    n = len(dab.genes)
    pairs_per_set = (set_size*(set_size-1))/2
    for i in range(samples):
        set_idxs = np.random.randint(n, size=(set_size))
        total_weight = dab.weights[np.ix_(set_idxs, set_idxs)].sum()
        diag_weight = dab.weights[set_idxs, set_idxs].sum()
        adjusted_weight = (total_weight - diag_weight)/2
        results.append(adjusted_weight/pairs_per_set)
    return results

def discard_outliers(data):
    """Filters a data list by removing any datapoints that are more than 1.5 * the distance
       between 25th and 75th percentiles away from either the 25th or 75th percentile"""
    lo, hi = np.percentile(data, [25, 75])
    lo_cutoff = lo - 1.5*(hi-lo)
    hi_cutoff = hi + 1.5*(hi-lo)
    return [d for d in data if lo_cutoff <= d <= hi_cutoff]

def filter_gene_sets(dab, gene_sets, size_range=(2, 150)):
    """Filter gene sets to only include genes listed in dab.genes. Optionally filter 
       to only include gene sets from size_range=(min_size, max_size). Substitute None
       for one edge of range to leave unconstrained"""
    all_genes = set(dab.genes)
    gene_sets = {k: [g for g in v if g in all_genes] for (k, v) in gene_sets.iteritems()}
    if size_range[0]:
        gene_sets = {k: v for (k,v) in gene_sets.iteritems() if len(v) >= size_range[0] }
    if size_range[1]:
        gene_sets = {k: v for (k,v) in gene_sets.iteritems() if len(v) <= size_range[1] }
    return gene_sets

def mean_coannotated_weight(dab, gene_to_idx, gene_set):
    """Calculates the average weight between any two coannotated genes from gene_set.
       The map gene_to_idx is defined such that if dab.genes[i] == gene, 
       then gene_to_idx[gene] == i"""
    set_idxs = [gene_to_idx[g] for g in gene_set if g in gene_to_idx]
    n = len(set_idxs)
    total_weight = dab.weights[np.ix_(set_idxs, set_idxs)].sum()
    diag_weight = dab.weights[set_idxs, set_idxs].sum()
    adjusted_weight = (total_weight - diag_weight)/2
    return adjusted_weight / ((n*(n-1))/2)

def get_coannotated_weight(dab, gene_sets):
    """Returns a frame with the average coannotated weight for genes co-annotated to GO terms"""
    #Make the DataFrame
    f = pd.DataFrame({"go_id":gene_sets.keys()})
    f['size'] = [len(gene_sets[r.go_id]) for r in f.itertuples()]
    gene_sets = [gene_sets[term] for term in f['go_id']]
    gene_to_idx = {g: i for i, g in enumerate(dab.genes)}

    f['coannotated_weight'] = map(
        lambda gene_set: mean_coannotated_weight(dab, gene_to_idx, gene_set),
        gene_sets)
    
    return f

def test_internal_connectivity(transforms, test_cases, bp_terms, cc_terms):
    """Takes in an array of GraphTransform objects, and applies each transform to
       each case in test_cases, which should be a SavedListGenerator"""
    global connectivity_results
    columns = ['test_case', 'test_number', 'transform', 'term_type', 'go_id', 'size', 'coannotated_weight',
                   'experimental_pval', 'calculated_pval']
    results = pd.DataFrame([], columns=columns)
    test_number = 0
    while test_cases.hasNext():
        test_number += 1
        dab, extra_info = test_cases.generateTest()
        for t in transforms:
            print "Internal Connectivity for %s" % t.name

            transformed_dab = t.transform(dab)
            distribution = {}
            print "Calculating Distribution:"
            samples = 1000
            for i in range(2,151):
                if i%10 == 0:
                    print i,
                distribution[i] = internal_connectivity_distribution(transformed_dab, i, samples=samples)
                distribution[i] = sorted(discard_outliers(distribution[i]))
            mean = get_linear_weights(transformed_dab).mean()
            distribution_std = {i: np.std(distribution[i]) for i in distribution}
            norm_statistic = [0]*151
            norm_pval = [0]*151
            for i in range(2,151):
                norm_statistic[i], norm_pval[i] = scipy.stats.normaltest(distribution[i])
            print "\n%d out of %d distributions look normal" % (len([p for p in norm_pval[2:] if p > 0.05]), len(distribution))

            #Run BP tests
            bp_terms_filt = filter_gene_sets(transformed_dab, bp_terms)
            f = get_coannotated_weight(transformed_dab, bp_terms_filt)
            f['transform'] = t.name
            f['test_case'] = test_cases.name
            f['term_type'] = 'BP'
            f['experimental_pval'] = [
                (samples - np.searchsorted(distribution[size], weight))/float(samples) \
                for (size, weight) in zip(f['size'], f['coannotated_weight'])
            ]    
            std = [distribution_std[size] for size in f['size']]
            f['calculated_pval'] = scipy.stats.norm.sf(f['coannotated_weight'], loc=mean, scale=std)
            f['test_number'] = test_number
            results = results.append(f)[columns]
            connectivity_results = connectivity_results.append(f)[columns]
            # Run CC tests
            cc_terms_filt = filter_gene_sets(transformed_dab, cc_terms)
            f = get_coannotated_weight(transformed_dab, cc_terms_filt)
            f['transform'] = t.name
            f['test_case'] = test_cases.name
            f['term_type'] = 'CC'
            f['experimental_pval'] = [
                (samples - np.searchsorted(distribution[size], weight))/float(samples) \
                for (size, weight) in zip(f['size'], f['coannotated_weight'])
            ]    
            std = [distribution_std[size] for size in f['size']]
            f['calculated_pval'] = scipy.stats.norm.sf(f['coannotated_weight'], loc=mean, scale=std)
            f['test_number'] = test_number
            results = results.append(f)[columns]
            connectivity_results = connectivity_results.append(f)[columns]
            print ""
    return results

connectivity_results = pd.DataFrame()





#### Testing GO Term similarity

def generate_jaccard(go_terms):
    genes = set()
    for gene_list in go_terms.itervalues():
        genes |= set(gene_list)
    genes = list(genes)
    n = len(genes)
    gene_to_idx = {g: i for i, g in enumerate(genes)}
    terms = go_terms.keys()
    
    # Calculate the intersection size between genes
    gene_to_term_matrix = np.zeros((len(genes), len(terms)), dtype=np.float64)
    for i, t in enumerate(terms):
        for g in go_terms[t]:
            gene_to_term_matrix[gene_to_idx[g], i] = 1.0
    intersection_size = np.dot(gene_to_term_matrix, gene_to_term_matrix.T)
    
    # Calculate the total number of annotations per gene
    annotation_count = np.zeros(n, dtype=np.float64)
    for t in terms:
        gene_idxs = [gene_to_idx[g] for g in go_terms[t]]
        annotation_count[gene_idxs] += 1
    
    # The union size is just the sum the individual annotation counts 
    # minus the size of the intersection
    union_size = np.add(annotation_count.reshape((n, 1)),
                        annotation_count.reshape((1, n))) - intersection_size
    
    ret = Dab()
    ret.genes = genes
    ret.weights = intersection_size/union_size
    return ret

def generate_overlap(go_terms):
    genes = set()
    for gene_list in go_terms.itervalues():
        genes |= set(gene_list)
    genes = list(genes)
    n = len(genes)
    gene_to_idx = {g: i for i, g in enumerate(genes)}
    terms = go_terms.keys()
    
    # Calculate the intersection size between genes
    # Calculate the intersection size between genes
    gene_to_term_matrix = np.zeros((len(genes), len(terms)), dtype=np.float64)
    for i, t in enumerate(terms):
        for g in go_terms[t]:
            gene_to_term_matrix[gene_to_idx[g], i] = 1.0
    intersection_size = np.dot(gene_to_term_matrix, gene_to_term_matrix.T)
    
    annotation_count = np.zeros(n, dtype=np.float64)
    for t in terms:
        gene_idxs = [gene_to_idx[g] for g in go_terms[t]]
        annotation_count[gene_idxs] += 1
    
    # Just take the minimum size between two genes
    min_size = np.minimum(annotation_count.reshape((n, 1)),
                          annotation_count.reshape((1, n)))
    
    ret = Dab()
    ret.genes = genes
    ret.weights = intersection_size/min_size
    return ret

#Note: before calling generate_semantic_density, filter the go_terms so that
#only genes from the Dab of interest are included
def generate_semantic_density(go_terms):
    # Get the count of all genes in go_terms
    genes = set()
    for gene_list in go_terms.itervalues():
        genes |= set(gene_list)
    genes = list(genes)  
    n = len(genes)
    terms = go_terms.keys()
    
    gene_to_idx = {g: i for i, g in enumerate(genes)}
    
    gene_to_term_matrix = np.zeros((len(genes), len(terms)), dtype=np.float64)
    gene_to_term_idx = {g:[] for g in genes}
    for i, t in enumerate(terms):
        score = -np.log10(len(go_terms[t])/float(n))
        for g in go_terms[t]:
            gene_to_term_matrix[gene_to_idx[g], i] = score
            gene_to_term_idx[g].append(i)
    
    numerator = np.zeros((n,n))
    # For each gene in the genome, calculate the scores connecting to it
    for i in range(n):
        term_idxs = gene_to_term_idx[genes[i]]
        if all(len(go_terms[terms[i]]) == n for i in term_idxs):
            continue
        numerator[i,i:] = gene_to_term_matrix[i:, term_idxs].max(axis=1)
    # Fill in the transpose side
    for i in range(n):
        numerator[i:,i] = numerator[i,i:]
    
    # The self scores for each term are just the scores along the diagonal
    self_scores = numerator[range(n), range(n)]
    denominator = np.add(self_scores.reshape((n, 1)),
                         self_scores.reshape((1, n)))
    
    ret = Dab()
    ret.genes = genes
    ret.weights = np.where(denominator > 0, numerator * 2 / denominator, 0)
    return ret


def correlation_and_overlap_cutoff(dab_weights, similarity_weights, similarity_name, cutoff):
    """Calculates the correlation and overlap between dab and semantic similarity weights
       with values below the `cutoff` percentile discarded (cutoff should be in range [0,100]. 
       dab_weights should be in sorted order and similarity_weights should be ordered 
       corresponding to the dab_weights"""
    n = int(len(dab_weights)*(cutoff*0.01))
    cutoff_value = dab_weights[n]
    # Do this to handle the case of many tied weights (like from KNN transform)
    n_idx = np.searchsorted(dab_weights, cutoff_value)
    pearson, _ = scipy.stats.pearsonr(dab_weights[n_idx:], similarity_weights[n_idx:])
    spearman, _ = scipy.stats.spearmanr(dab_weights[n_idx:], similarity_weights[n_idx:])
    similarity_cutoff_value = np.percentile(similarity_weights, cutoff)
    top_dab_weights = dab_weights >= cutoff_value
    top_go_weights = similarity_weights >= similarity_cutoff_value
    return {
        'similarity_measure': similarity_name,
        'cutoff_percentile': cutoff,
        'cutoff_value': cutoff_value,
        'pearson': pearson,
        'spearman': spearman,
        'total_edges': len(dab_weights),
        'network_edges': top_dab_weights.sum(),
        'similarity_edges': top_go_weights.sum(),
        'overlapping_edges': np.logical_and(top_dab_weights, top_go_weights).sum()
    }
    
    
def test_semantic_weights(transforms, test_cases, bp_terms, cc_terms, cutoffs=[0,50,80,90,95,99]):
    """Takes in an array of GraphTransform objects, and applies each transform to
       each case in test_cases, which should be a SavedListGenerator"""
    global semantic_weight_results
    columns = ['test_case', 'test_number', 'transform', 'term_type', 'similarity_measure',
               'cutoff_percentile', 'cutoff_value', 'pearson', 'spearman', 'total_edges',
               'network_edges', 'similarity_edges', 'overlapping_edges', 'fold_enrichment']
    results = pd.DataFrame([], columns=columns)
    test_number = 0
    while test_cases.hasNext():
        test_number += 1
        dab, extra_info = test_cases.generateTest()
        for t in transforms:
            transformed_dab = t.transform(dab)
            bp_filtered = filter_gene_sets(transformed_dab, bp_terms, size_range=(2,None))
            cc_filtered = filter_gene_sets(transformed_dab, cc_terms, size_range=(2,None))
            # Run BP tests
            jaccard_bp = generate_jaccard(bp_filtered)
            overlap_bp = generate_overlap(bp_filtered)
            semantic_bp = generate_semantic_density(bp_filtered)
            assert set(jaccard_bp.genes) <= set(transformed_dab.genes)
            assert set(jaccard_bp.genes) == set(overlap_bp.genes)
            assert set(overlap_bp.genes) == set(semantic_bp.genes)
            genes = list(set(jaccard_bp.genes) & set(transformed_dab.genes))
            dab_weights = get_linear_weights(transformed_dab.subset_genes(genes))
            dab_order = np.argsort(dab_weights)
            dab_weights = dab_weights[dab_order]
            jaccard_bp_weights = get_linear_weights(jaccard_bp.subset_genes(genes))[dab_order]
            overlap_bp_weights = get_linear_weights(overlap_bp.subset_genes(genes))[dab_order]
            semantic_bp_weights = get_linear_weights(semantic_bp.subset_genes(genes))[dab_order]
            r = pd.DataFrame([
                correlation_and_overlap_cutoff(dab_weights, similarity_weights, name, cutoff)
                for cutoff in cutoffs
                for name, similarity_weights in zip(
                    ["Jaccard Index", "Overlap Coefficient", "Semantic Density"],
                    [jaccard_bp_weights, overlap_bp_weights, semantic_bp_weights])
            ])
            background_rate = r['similarity_edges']/r['total_edges']
            enriched_rate = r['overlapping_edges']/r['network_edges']
            r['fold_enrichment'] = enriched_rate/background_rate
            r['term_type'] = 'BP'
            r['transform'] = t.name
            r['test_number'] = test_number
            r['test_case'] = test_cases.name
            results = results.append(r)[columns]
            semantic_weight_results = semantic_weight_results.append(r)[columns]
            # Run CC tests
            jaccard_cc = generate_jaccard(cc_filtered)
            overlap_cc = generate_overlap(cc_filtered)
            semantic_cc = generate_semantic_density(cc_filtered)
            assert set(jaccard_cc.genes) == set(overlap_cc.genes)
            assert set(overlap_cc.genes) == set(semantic_cc.genes)
            genes = list(set(jaccard_cc.genes) & set(transformed_dab.genes))
            dab_weights = get_linear_weights(transformed_dab.subset_genes(genes))
            dab_order = np.argsort(dab_weights)
            dab_weights = dab_weights[dab_order]
            jaccard_cc_weights = get_linear_weights(jaccard_cc.subset_genes(genes))[dab_order]
            overlap_cc_weights = get_linear_weights(overlap_cc.subset_genes(genes))[dab_order]
            semantic_cc_weights = get_linear_weights(semantic_cc.subset_genes(genes))[dab_order]
            r = pd.DataFrame([
                correlation_and_overlap_cutoff(dab_weights, similarity_weights, name, cutoff)
                for cutoff in cutoffs
                for name, similarity_weights in zip(
                    ["Jaccard Index", "Overlap Coefficient", "Semantic Density"],
                    [jaccard_cc_weights, overlap_cc_weights, semantic_cc_weights])
            ])
            background_rate = r['similarity_edges']/r['total_edges']
            enriched_rate = r['overlapping_edges']/r['network_edges']
            r['fold_enrichment'] = enriched_rate/background_rate
            r['term_type'] = 'CC'
            r['transform'] = t.name
            r['test_number'] = test_number
            r['test_case'] = test_cases.name
            results = results.append(r)[columns]
            semantic_weight_results = semantic_weight_results.append(r)[columns]
    return results

semantic_weight_results = pd.DataFrame()




### TESTING MODULARITY

def get_modularity(dab, extra_info):
    graph = igraph.Graph.Weighted_Adjacency(dab.weights.tolist(), mode=igraph.ADJ_UPPER, loops=False)
    graph.vs['name'] = dab.genes
    go_terms = extra_info['original_genesets']
    gene_to_go = {}
    seen = set()
    for term, gene_list in go_terms.iteritems():
        assert len(seen & set(gene_list)) == 0
        seen |= set(gene_list)
        for g in gene_list:
            gene_to_go[g] = term
    graph.vs['GO_Term'] = [gene_to_go[v['name']] for v in graph.vs]
    clustering = igraph.VertexClustering.FromAttribute(graph, 'GO_Term')
    return {
        'gene_sets': len(go_terms),
        'n_genes': len(graph.vs),
        'modularity': clustering.modularity
    }

def test_modularity(transforms, test_cases):
    global modularity_results
    columns = ['test_case', 'test_number', 'transform', 'gene_sets', 'n_genes', 'modularity']
    results = pd.DataFrame([], columns=columns)
    test_number = 0
    while test_cases.hasNext():
        test_number += 1
        dab, extra_info = test_cases.generateTest()
        for t in transforms:
            transformed_dab = t.transform(dab)
            m = get_modularity(transformed_dab, extra_info)
            m['test_case'] = test_cases.name
            m['test_number'] = test_number
            m['transform'] = t.name
            f = pd.DataFrame([m], columns=columns)
            results = results.append(f)[columns]
            modularity_results = modularity_results.append(f)[columns]
    return results

modularity_results = pd.DataFrame()