import numpy as np
import pandas as pd

import sys
sys.path.append('../lib')
from Dab import Dab

import igraph


# Define the core classes to run graph preprocessing, clustering, 
# and performance testing


"""
Dream API:

data_frame = run_tests(transform = [PearsonCorrelate(), KNN(k = 50)],
                          cluster = Louvain(),
                          metric = [AdjustedRand(), AnnotationCorrespondence()],
                          test_generator = NonOverlappingGOTerms(dab, go_terms),
                          independent_var = "term_size",
                          independent_var_values = [10, 50, 100, 150])

... do some filtering to the data frame ...
make_boxplot(data_frame)

How it works: You specify a pipeline of transforms to run before your clustering step,
              then you specify your clustering algorithm, and the metrics you would like
              to collect from each test.

              The specified test generator is responsible for making subsetted Dab files
              with a gold-standard clustering answer.

              independent_var specifies a keyword argument to the test_generator that should
              get varied, and the values given are tested in sequence

              returns a pandas DataFrame with a row for each trial, and a column for each metric
              and independent variable

Pros: 
    - Can swap in and out algorithms quickly
    - Not a lot of code to make a new test
    - Class instantiation can be used as a way of partial application as well as setting algorithm parameters

Cons:
    - No clear way to make more than one independent variable
    - A fair amount of ceremony (but I think worthwhile for the structure)
    - Can't run *all* types of tests:
        - Testing how preprocessing affects modularity
                (Could do with transform list + test_generator + metric. Just take out clustering)
        - Looking at clustering consistency in the face of data perturbation or query set perturbation
                (Key problem is comparing test clusterings against each other, perturbation can be done with
                 a test_generator)
        - Also, how do test_generators work? Do they allow for test types with and without gold standards?

Other API idea:
    - accept arguments:
        - transform, cluster, metric, test_generator
        - All arguments can be a single object or a list of objects. If list of objects, then treat as an 
          independent variable, and run all combinations.
    Pros:
        - Handles inclusion of independent variables smoothly in *any* context
    Cons:
        - For making charts, it won't be as obvious how to label things: When given a list of objects,
          how do you know what the thing that's being varied is (Both the name, and what the distinct values are)

New Dream API:

generators = [NonOverlappingGOTerms(dab, go_terms, term_size=(k,k*2)) for k in [10, 50, 100, 150]]

data_frame = run_tests(transform = ChainTransform(PearsonCorrelate(), KNN(k = 50)),
                          cluster = Louvain(),
                          metrics = [AdjustedRand(), AnnotationCorrespondence()],
                          test_generator = IndependentVar(name="Term Size", values=generators))

"""

# Transform a graph adjacency matrix as a preprocessing step to clustering
class GraphTransform:
    name = 'None'

    def __init__(self, **kwargs):
        pass

    # Take in a Dab object, and return a Dab object with transformed 
    # weights
    def transform(self, dab, **kwargs):
        return dab

# Cluster a graph
class GraphCluster:
    name = 'None'

    def __init__(self, **kwargs):
        pass
    
    # Take in a Dab object, and
    # return a VertexClustering object
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        # Just put all the nodes into one cluster for the default implementation
        clust = igraph.VertexClustering(graph, membership=[0]*graph.vcount())
        return clust
    
    # Convert a Dab into a weighted igraph
    @staticmethod
    def dab_to_graph(dab):
        graph = igraph.Graph.Weighted_Adjacency(dab.weights.tolist(), mode=igraph.ADJ_UPPER, loops=False)
        graph.vs['name'] = dab.genes
        return graph

class Louvain(GraphCluster):
    name = 'Louvain'

    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_multilevel(weights='weight')

# Make clustering algorithms for the built-in igraph clustering algorithms
class FastGreedy(GraphCluster):
    name = 'Greedy Modularity'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_fastgreedy(weights='weight').as_clustering()

class Infomap(GraphCluster):
    name = 'Infomap'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_infomap(edge_weights='weight')

class LeadingEigenvector(GraphCluster):
    name = 'Leading Eigenvector'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_leading_eigenvector(weights='weight')

class LabelPropagation(GraphCluster):
    name = 'Label Propagation'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_label_propagation(weights='weight')

class EdgeBetweenness(GraphCluster):
    name = 'Edge Betweenness'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_edge_betweenness(weights='weight').as_clustering()

class Spinglass(GraphCluster):
    name = 'Spinglass'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_spinglass(weights='weight')

class Walktrap(GraphCluster):
    name = 'Walkttrap'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_walktrap(weights='weight').as_clustering()


# Evaluate a given clustering compared to a gold-standard clustering
class ClusterMetric:
    name = 'None'
    def __init__(self, **kwargs):
        pass

    # Take in two VertexClustering objects and return a floating point number
    def score(self, clustering, **kwargs):
        return 0

class Modularity(ClusterMetric):
    name = 'Modularity'
    def score(self, clustering, **kwargs):
        return clustering.modularity

class ClusterCount(ClusterMetric):
    name = 'Number of Clusters'
    def score(self, clustering, **kwargs):
        return len(clustering)

class ClusterSizes(ClusterMetric):
    name = 'Cluster Sizes'
    def score(self, clustering, **kwargs):
        sizes = map(str, sorted(clustering.sizes()))
        return ','.join(sizes)

class GoldStandardMetric(ClusterMetric):
    def _get_gold_standard(self, clustering, original_genesets):
        
        def get_gene_cluster(gene):
            for i, t in enumerate(original_genesets):
                if gene in original_genesets[t]:
                    return i

        g = clustering.graph
        gold_standard = igraph.VertexClustering(
            graph=g, 
            membership=[get_gene_cluster(gene) for gene in g.vs['name']]
        )

        return gold_standard

class AdjustedRand(GoldStandardMetric):
    name = 'Adjusted Rand Index'
    def score(self, clustering, original_genesets, **kwargs):
        gold_standard = self._get_gold_standard(clustering, original_genesets)
        return gold_standard.compare_to(clustering, method="adjusted_rand")

class TestGenerator:
    name = 'None'
    def __init__(self, **kwargs):
        pass
    
    def generateTest(self, **kwargs):
        pass


# Generates a randomly selected set of N genes
class RandomSet(TestGenerator):
    name = 'Random'
    def __init__(self, dab, n, **kwargs):
        self.n = n
        self.dab = dab
    
    def generateTest(self, **kwargs):
        gene_list = np.random.choice(self.dab.genes, size=self.n, replace=False)
        return (self.dab.subset_genes(gene_list), {})

# Generates a randomly selected list of non-overlapping GO Terms.
# Method: Selects a random set of GO terms, then takes out any genes annotated to more than one term.
#         If removing duplicates keeps all sets above the minimum size, then return the sets, otherwise
#         try again
# Parameters:
# all_terms: map from term name to a list of gene names, as returned from read_gmt
# n: the number of terms desired
# size_range: a tuple of (min_size, max_size) specifying what size GO Terms to look for
# break_point: how many attempts to make at finding non-overlapping terms before giving up
class NonOverlappingGOTerms(TestGenerator):
    _defaults = {
        'size_range': (1,100000), 
        'break_point':1000, 
        'avoid_repeats':True,
    }

    def __init__(self, dab, all_terms, **kwargs):
        self.dab = dab
        # Filter the list of GO terms to only include genes that are in the Dab
        dab_genes = set(dab.genes)
        self.all_terms = {k:[g for g in gene_list if g in dab_genes] for (k,gene_list) in all_terms.iteritems()}
        self._defaults.update(kwargs)
    
    def _remove_overlaps(self, terms):
        duplicates = set()
        seen = set()
        for t in terms.values():
            duplicates = seen & t
            seen = seen | t
        return {k: v - duplicates for (k,v) in terms.iteritems()}

    def _get_nonoverlapping_terms(self, **kwargs):
        params = self._defaults.copy()
        params.update(kwargs)
        
        min_size, max_size = params['size_range']
        go_terms = {k:set(v) for (k,v) in self.all_terms.iteritems()
                    if min_size <= len(v) <= max_size}
        
        for i in range(params['break_point']):
            term_names = np.random.choice(go_terms.keys(), size=params['n'])
            terms = {name: go_terms[name] for name in term_names}
            non_overlapping_terms = self._remove_overlaps(terms)
            if all(len(term) > min_size for term in non_overlapping_terms.itervalues()):
                return non_overlapping_terms
        
        raise Exception("Too many failed iterations while attempting to make non-overlapping terms (%d)" % break_point)
    
    def generateTest(self, **kwargs):
        terms = self._get_nonoverlapping_terms(**kwargs)
        all_genes = []
        for term in terms.values():
            all_genes += list(term)
        
        return (self.dab.subset_genes(all_genes), {'original_genesets': terms})

# A class that represents an independent variable in a clustering experiment
import itertools

class IndependentVar:
    def __init__(self, name, values, value_labels=None):
        self.name = name
        self.values = values
        if value_labels:
            assert len(values) == len(value_labels)
            for i in range(len(values)):
                values[i].name = value_labels[i]
    
    # Iterates through the values of an IndependentVar.
    # If passed an instance of IndependentVar, iterates through
    # the objects in the value array, yielding 2-length tuples
    # of shape (object, {'name': 'value_label'}.
    # For other objects, just yields the value with an empty dictionary
    # as the second tuple element.
    @staticmethod
    def iterate(var):
        if isinstance(var, IndependentVar):
            for v in var.values:
                yield (v, {var.name: v.name})
        else:
            yield (var, {})


# Runs a set of clustering tests using a giving transform, clustering algorithm, and test generator,
# while collecting the results in a pandas data frame 
def run_clustering_tests(test_generator, transform, cluster, metrics, tests_per_setup=10):
    #Set up columns to initialize data frame    
    columns = []

    # Check whether each parameter provides an independent variable
    for param in [test_generator, transform, cluster]:
        if isinstance(param, IndependentVar):
            columns.append(param.name)
    
    # Add columns for each metric
    for m in metrics:
        columns.append(m.name)

    frame = pd.DataFrame(columns = columns)

    for gen, gen_label in IndependentVar.iterate(test_generator):
        for i in range(tests_per_setup):
            dab, metric_kwargs = gen.generateTest()
            for trans, trans_label in IndependentVar.iterate(transform):
                transformed_dab = trans.transform(dab)
                for clust, clust_label in IndependentVar.iterate(cluster):
                    clustering = clust.cluster(transformed_dab)
                    results = {m.name: m.score(clustering, **metric_kwargs) for m in metrics}
                    results.update(gen_label)
                    results.update(trans_label)
                    results.update(clust_label)
                    frame = frame.append([results], ignore_index=True)

    return frame


"""
from clustering import *
import numpy as np
import pandas as pd

import sys
sys.path.append('../lib')
from Dab import Dab
from Gmt import read_gmt

import igraph

dab = Dab("../data/brain.dab")
go_terms = read_gmt('../gene_ontology/gobp_human.closed.gmt')

term_sizes = [10, 50, 100]
generators = [NonOverlappingGOTerms(go_terms, dab, term_size=(k,k*2), n=2) for k in term_sizes]

frame = run_clustering_tests(IndependentVar("Term Size", generators, term_sizes), GraphTransform(), Louvain(), [AdjustedRand()])

"""
"""



data_frame = run_tests(transform = ChainTransform(PearsonCorrelate(), KNN(k = 50)),
                          cluster = Louvain(),
                          metrics = [AdjustedRand(), AnnotationCorrespondence()],
                          test_generator = IndependentVar(name="Term Size", values=generators))

"""