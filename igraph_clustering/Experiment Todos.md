# Experiment Todos

This is a comprehensive listing of all the experiments I want to run to measure speed, robustness, and quality of various clustering + transform combinations.

### Transform Combinations:

Neighbor Measures:

- KNN
- Topological Overlap (GTOM1)

Bulk Transforms:

- Pearson Correlation
- Spearman Correlation
- Cosine Distance

Try each transform individually, then pair between categories using both orders. 
(This results in 17 different options for combinations!)

(What about thresholding as a transform? Or log transform? Should maybe worry about that)  
(Also consider all pairs shortest paths)

### Clustering algorithms:

For now, we'll just focus on 8 algorithms that are in igraph, ignoring heirarchical clustering, etc.

- Greedy modularity optimization
- Infomap
- Label Propagation
- Louvain
- Edge Betweenness
- Spinglass
- Walktrap

### Test Case Generators:

For now, I want to avoid doing really massive test cases, because there's not much point to adding that to GIANT. So the goal will be to do clusterings that are up to size 2600 -- just over 10% of the genome

The obvious test case generators are:

- Randomly selected genes
    * Gene counts: 10, 50, 100, 500, 1000, 2600
- GO Term-based selection
    * I'm a lot less certain how possible it will be to put together big test cases with GO Terms, since 
    there's the obvious issue of needing largely non-overlapping terms. But here's an ideal list
    * Get total gene counts in range of 20, 50, 100, 500, 1000, 2600
    * Compose each test set of GO terms of size 10, 50, and 100
    * If this all works, that would make 18 types of tests
- Real queries from Arjun
    * Need to ask about getting these

### Tests to run:

In general these tests will be run 10 times per setup as long as each test takes under 1 minute to run. 5 times per setup for tests up to 2 minutes to run. 1 time for tests up to 5 minutes to run.

- Speed + Quality tests:
    * Just run all combinations of Transforms, Clustering Algs, and Test Case generators
    * With randomly selected genes, measure: modularity, number of clusters, cluster sizes
    * With GO Terms, measure all that plus all the built-in cluster comparisons, plus my "Annotation Correspondence" metric

- Robustness tests:
    * Replace x% of gene set with random genes: 
        + try 1%, 5%, 10%, 25%
        + or maybe try 2% increments averaged 10 times from 0% to 100% -> That's 500 Clusterings to run!
    * Perturb the input weights with gaussian noise:
        + add noise to the data from 0 to 0.5