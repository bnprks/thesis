def get_nonoverlapping_terms2(all_terms, total_size, term_size_range=(1,1000), 
                             attempts=5, max_pairwise_overlap=0.1, max_global_overlap=0.25):
    
    min_size, max_size = term_size_range
    go_terms = {k: set(v) for (k,v) in all_terms.iteritems()
                if min_size <= len(v) <= max_size}
    
    # Given a list of term names, returns two sets (duplicates, all_genes)
    # duplicates is a set of duplicated genes; all_genes is the union of all the genes
    def get_duplicates(term_names):
        duplicates = set()
        all_genes = set()
        for t_name in term_names:
            t = go_terms[t_name]
            duplicates |= seen & t
            seen |= t
        return duplicates, all_genes
    
    # Given a list of term names and initial conflicts, returns a list of terms that
    # can be removed to make the whole list satisfy the max_global_overlap requirement
    def get_conflicting_terms(term_names, initial_conflicts):
        conflicts = set(initial_conflicts)
        no_removed_genes = False
        while not no_removed_genes:
            term_names = [t for t in term_names if t not in conflicts]
            duplicates, all_genes = get_duplicates(term_names)
            no_removed_genes = True
            for t in np.random.permutation(term_names):
                if t in conflicts:
                    continue
                if len(go_terms[t] & duplicates) > len(go_terms[t]) & max_global_overlap:
                    conflicts.add(t)
                    no_removed_genes = False
                    break
        return list(conflicts)
            
    
    #Try the randomized process `attemts` times
    for i in range(attempts):
        chosen_terms = []
        for term in np.random.permutation(go_terms.keys()):
            duplicates, all_genes = get_duplicates(chosen_terms)
            # Check if we've gotten enough genes
            if len(all_genes) >= total_size:
                return chosen_terms
            
            new_genes = go_terms[term]
            # Check if the new term violates global overlap threshold
            if len(new_genes & all_genes) > len(new_genes) * max_global_overlap: 
                continue
            
            # List of terms that would need to be removed if new term is added
            conflicting_terms = [] 
            # Whether the current term has a pairwise conflict
            has_pairwise_conflict = False
            for t_name in chosen_terms:
                pairwise_overlap = len(new_genes & go_terms[t_name])
                if pairwise_overlap > len(go_terms[t_name]) * max_pairwise_overlap:
                    conflicting_terms.append(t_name)
                if pairwise_overlap > len(new_genes) * max_pairwise_overlap:
                    has_pairwise_conflict = True
                    break
            if has_pairwise_conflict:
                continue
            
            # Find how many genes would need to be removed to add this gene
            conflicting_terms = get_conflicting_terms(chosen_terms + [term], conflicting_terms)
            genes_to_remove = sum(len(go_terms[t]) for t in conflicting_terms)
            genes_to_add = len(new_genes - all_genes)
            # With probability genes_to_add/(genes_to_add + genes_to_remove) add the new term
            if np.random.random() < float(genes_to_add)/(genes_to_add+genes_to_remove):
                chosen_terms.append(term)
                chosen_terms = [t for t in chosen_terms if t not in conflicting_terms]
            
        #print "Failed iteration: %d sets found" % len(chosen_terms)
    
    raise Exception("Too many failed iterations while attempting to make non-overlapping terms (%d)" % attempts)

def get_nonoverlapping_terms(all_terms, total_size, term_size_range=(1,1000), 
                             attempts=5, max_pairwise_overlap=0.1, max_global_overlap=0.25):
    min_size, max_size = term_size_range
    go_terms = {k: set(v) for (k,v) in all_terms.iteritems()
                if min_size <= len(v) <= max_size}
    
    def can_add_term(chosen_terms, new_term):
        new_genes = go_terms[new_term]
        for t in chosen_terms:
            t_genes = go_terms[t]
            if len(t_genes & new_genes) > max_overlap*len(new_genes):
                return False
        return True
    
    def get_duplicated_genes(term_names):
        duplicates = set()
        seen = set()
        for t_name in term_names:
            t = go_terms[t_name]
            duplicates |= seen & t
            seen = seen | t
        duplicates
    
    def can_add_term2(chosen_terms, new_term):
        all_genes = reduce(lambda x, y: x|y, [go_terms[t] for t in chosen_terms])
        new_set = go_terms[new_term]
        #Check the new term doesn't violate global or pairwise ovelap thresholds
        if len(new_set & all_genes) > max_global_overlap * len(new_set):
            return False
        for t in chosen_terms:
            if len(new_set & go_terms[t]) > max_pairwise_overlap * len(new_set):
                return False
        #Check that we won't have to eliminate more genes than we're adding by doing th
        all_genes |= new_set
        duplicates = get_duplicated_genes(chosen_terms + [new_term])
        added_genes = len(new_set - all_genes)
        lost_genes = 0
        lost_terms = []
        for t in chosen_terms:
            if len(duplicates & go_terms[t]) > max_pairwise_overlap * len(go_terms[t]):
                lost_genes += len()
            
        
    
    #Try the randomized process `attemts` times
    for i in range(attempts):
        terms = go_terms.keys()
        chosen_terms = []
        #Try adding random terms iteratively until 
        while len(terms) > 0 and sum(len(go_terms[t]) for t in chosen_terms) < total_size:
            t_idx = np.random.randint(0, len(terms))
            terms[-1], terms[t_idx] = terms[t_idx], terms[-1]
            t = terms.pop()
            if can_add_term(chosen_terms, t):
                chosen_terms.append(t)
        
        if sum(len(go_terms[t]) for t in chosen_terms) >= total_size:
            return chosen_terms
        else:
            print "Failed iteration: %d sets found totalling %d genes" % (
                len(chosen_terms),
                sum(len(go_terms[t]) for t in chosen_terms)
            )
    
    raise Exception("Too many failed iterations while attempting to make non-overlapping terms (%d)" % attempts)
            
def remove_overlaps(terms):
    duplicates = set()
    seen = set()
    for t in terms.values():
        duplicates |= seen & t
        seen = seen | t
    return {k: v - duplicates for (k,v) in terms.iteritems()}
    
def _get_nonoverlapping_terms(self, **kwargs):
        params = self._defaults.copy()
        params.update(kwargs)
        
        min_size, max_size = params['size_range']
        go_terms = {k:set(v) for (k,v) in self.all_terms.iteritems()
                    if min_size <= len(v) <= max_size}
        
        for i in range(params['break_point']):
            term_names = np.random.choice(go_terms.keys(), size=params['n'])
            terms = {name: go_terms[name] for name in term_names}
            non_overlapping_terms = self._remove_overlaps(terms)
            if all(len(term) > min_size for term in non_overlapping_terms.itervalues()):
                return non_overlapping_terms
        
        raise Exception("Too many failed iterations while attempting to make non-overlapping terms (%d)" % break_point)