import numpy as np
import pandas as pd
from datetime import datetime

import sys
sys.path.append('../lib')
from Dab import Dab

start = datetime.now()
d = Dab('../data/brain.dab')
end = datetime.now()
print "Reading a .dab takes: ", (end - start)

mat = d.weights.astype(np.float32, casting='same_kind')
mat.tofile('../data/test_matrix.mat')

mat = None
d = None
start = datetime.now()
mat = np.fromfile('../data/test_matrix.mat', dtype=np.float32).astype(np.float64, casting='same_kind')
end = datetime.now()
print "Reading a matrix file with conversions takes: ", (end-start)