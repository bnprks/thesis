#Counts the number of NaN values in the .dab files passed as arguments
#Usage: dab_has_nan.py [file.dab]...

import numpy as np

import sys
sys.path.append('../lib')
from Dab import Dab


def main():
    if len(sys.argv) < 2:
        print "Usage: %s [file.dab]..." % sys.argv[0]
        return
    
    print "Number of NaNs per file"
    for fname in sys.argv[1:]:
        d = Dab(fname)
        print "%s: %d" % (fname, np.sum(np.isnan(d.weights)))

if __name__ == '__main__':
    main()