import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os.path

import sys
sys.path.append('../lib')
from Dab import Dab

def get_edge_hist(d, bins=100, range=None, title="Edge Weight Histogram"):
    N = len(d.genes)
    #Make a single array with each edge weight in it once
    all_edges = np.empty((N*(N-1))/2, dtype=np.float32)
    curidx = 0
    for i in xrange(N-1):
        all_edges[curidx:(curidx+N-i-1)] = d.weights[i, i+1:N]
        curidx += N-i-1
    #Do plotting
    plt.hist(all_edges, bins=bins, log=True)#range=range, log=True)
    plt.xlabel('Score')
    plt.ylabel('Edge Count')
    plt.title(title)

def get_degree_hist(d, bins=100, range=None, title="Gene Degree Histogram"):
    N = len(d.genes)
    for i in xrange(N):
        d.weights[i,i] = 0
    degrees = d.weights.sum(axis=1)
    #Do plotting
    plt.hist(degrees, bins=bins, log=True)#range=range, log=True)
    plt.xlabel('Degree')
    plt.ylabel('Gene Count')
    plt.title(title)

def two_plots(d):
    plt.subplot(1, 2, 1)
    get_edge_hist("Edge Weight")
    plt.subplot(1, 2, 2)
    get_degree_hist(d)

def main():
    if len(sys.argv) < 3:
        print "Usage: %s output.png [networks.dab]..." % sys.argv[0]
        return
    nrows = len(sys.argv) - 2
    abspaths = map(os.path.abspath, sys.argv[2:])
    #Get fancy with just showing as much pathname as necessary
    character_skip = os.path.commonprefix(abspaths).rfind(os.sep) + len(os.sep)
    

    plt.figure(figsize=(3*nrows,3*2)) #Set size to approximately letter
    
    for i in range(nrows):
        fname = abspaths[i]
        print_name = fname[character_skip:]
        d = Dab(fname)
        plt.subplot(nrows, 2, 2*i + 1)
        get_edge_hist(d, title="%s Edge Weights" % print_name)
        plt.subplot(nrows, 2, 2*i + 2)
        get_degree_hist(d, title="%s Gene Degrees" % print_name)

    plt.tight_layout()
    plt.savefig(sys.argv[1])

if __name__ == '__main__':
    main()
