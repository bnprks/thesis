import numpy as np

import sys
sys.path.append('../lib')
from Dab import Dab


def sort_dab_genes(d):
    """Takes an input Dab object, and reorders its columns and rows so that
       the gene names corresponding with each column are in sorted order. 
       At the end, the object is equivalent to the old one, just in a canonical
       ordering (for easier comparison between dabs holding the same genes)"""

    #Get the index reordering to have a sorted gene list
    permutation = sorted(range(len(d.genes)), key=lambda x: d.genes[x])
    new_weights = d.weights[:,permutation][permutation]
    d.weights = new_weights
    d.genes = sorted(d.genes)