import numpy as np
import pandas as pd

import sys
sys.path.append('../lib')
from Dab import Dab


#Investigating which columns create a NaN correlation -- looks like some columns set to all one value

#Get a few columns that correlate to 0 where I can put them into R
d = Dab('../data/brain.dab')
corr = Dab('../data/brain.cor.dab')

nulls = np.argwhere(np.isnan(corr.weights))
sample_idxs = np.random.random_integers(0, len(nulls), 5)
null_sample = list(set(nulls[sample_idxs].flatten()))

#Set the diagonal of d to nan for clarity
for i in range(len(d.genes)):
    d.weights[i,i] = np.nan

out_names = map(lambda x: d.genes[x], null_sample)
out_weights = d.weights[:, null_sample]
np.savetxt('nan_sample.csv', out_weights,delimiter=',', header=",".join(out_names))

#Checking what happens if we filter out the bad columns (with constant values)
corr_genes = corr.genes
corr = None

N = len(d.weights)

const_col = {}

for i in xrange(N):
    val = d.weights[i,0]
    if i == 0:
        val = d.weights[i,1]
    if all(np.isnan(x) or x==val for x in d.weights[i]):
        const_col[d.genes[i]] = val

rand_key = const_col.keys()[0]
all(x == const_col[rand_key] for x in const_col.itervalues())

all(corr_genes[a] in const_col or corr_genes[b] in const_col
        for (a,b) in nulls)

"""const_col.keys() == ['6960', '100131608', '8389', '653125', '100302182', '7118', '337874', '100131205', '100528064', '100422872', '100505385', '100422955', '81315', '645402', '100113381', '646309', '257468', '645175', '60358', '692213', '645836', '728255', '81431', '728710', '645078', '729428', '642425', '100506074', '729384', '100422961', '728619', '100422885', '100034743', '374677', '677847', '100526825', '100133142', '692201', '728132', '26687', '28834', '653259', '645587', '692199', '100310846', '100310847', '100133251', '390432', '100422879', '100532737', '390531', '406875', '100422991', '259293', '158078', '573971', '100288470', '100423025', '337873', '406975', '390134', '790969', '790968', '653420', '645619', '390231', '100422906', '653505', '729992', '79346', '81228', '100506250', '100528016', '100431172', '644974', '100302143', '100294512', '730291', '100534593', '619567', '100505593', '692224', '100187701', '100505852', '100529251', '100422910', '81193', '653045', '392188', '222901', '79274', '100528020', '100507444', '3873', '285849', '100423004', '100423009', '100423008', '100289017', '81458', '100422890', '100313887', '643615', '100422842', '100422844', '401447', '85295', '81169', '100128853', '100288406', '642489', '728447', '266656', '100288527', '440517', '3539', '392242', '100422935', '100506193', '388559', '100526773', '100526772', '79482', '6976', '388078', '732265', '8391', '100287887', '100170646', '653543', '100113400', '646000', '441056', '283110', '100422949', '100526767']"""

a_idx = d.genes.index('6960')
a = d.weights[a_idx]
b_idx = d.genes.index('8389')
b = d.weights[b_idx]

N = len(d.genes)
num = np.dot(a,b)*(N-2) - (np.sum(a) - d.weights[a_idx,b_idx]) * (np.sum(b) - d.weights[a_idx,b_idx])

denom_sq = ((N-2)*(np.sum(np.square(a)) - x**2) - (np.sum(a) - x)**2)* \
           ((N-2)*(np.sum(np.square(b)) - x**2) - (np.sum(b) - x)**2)


