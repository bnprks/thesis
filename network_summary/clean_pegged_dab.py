# Remove genes from a .dab file which are all pegged to a single value
# Usage: clean_pegged_dab.py input.dab output.dab

import numpy as np

import sys
sys.path.append('../lib')
from Dab import Dab


def main():
    if len(sys.argv) != 3:
        print "Usage: %s input.dab output.dab" % sys.argv[0]
        return

    d = Dab(sys.argv[1])
    N = len(d.genes)
    pegged_genes = set()

    #Set the diagonal of d to nan for clarity
    for i in range(len(d.genes)):
        d.weights[i,i] = np.nan

    for i in xrange(N):
        val = d.weights[i,0]
        if i == 0:
            val = d.weights[i,1]
        if all(np.isnan(x) or x==val for x in d.weights[i]):
            pegged_genes.add(i)
    print "Pegged genes: ", len(pegged_genes)

   
    keepers = [x for x in range(N) if x not in pegged_genes]

    d.genes = [d.genes[i] for i in keepers]
    d.weights = d.weights[:,keepers][keepers]

    d.write(sys.argv[2])

if __name__ == '__main__':
    main()