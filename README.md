# README #

This project contains clustering and preprocessing algorithms for functional interaction networks. The 'lib' folder contains a python library with classes for clustering and preprocessing algorithms. Other folders contain a series of Jupyter notebooks with analyses of clustering algorithms


# API outline
For the API, there are 4 main classes that define algorithms:

   - `GraphTransform`: defines a transform from one adjacency matrix to another
   - `GraphCluster`: defines a clustering algorithm 
   - `ClusterMetric`: defines a metric that can be measured on a clustered graph
   - `TestGenerator`: defines a test case generator for evaluating algorithm performance

Note that all public methods take a `**kwargs` for maximum forwards compatibility. For now,
the only realistic use-case is that when a TestGenerator that makes a gold standard, it 
will be able to pass that information to a ClusterMetric that might rely on knowing a gold standard.

Also note that these implementations are assumed to be very light-weight in terms of memory.
They may use more memory during computation, but their resting state should hold a bare minimum of data.

The class definitions are as follows:

### GraphTransform
*Properties*

   - name: a printable name of the graph transform 

*Public Methods*

   - `__init__(self, **kwargs)`: initialize the transformer with optional parameters
   - `transform(self, dab, **kwargs)`: returns a new Dab object formed by applying the transform to its argument.
      Weights should range from 0 to 1 in the transformed Dab, with 1 corresponding to strongly connected genes and
      0 corresponding to unconnected genes

### GraphCluster
*Properties*

   - `name`: a printable name of the clustering algorithm 

*Public Methods*

   - `__init__(self, **kwargs)`: initialize the clustering algorithm with optional parameters
   - `cluster(self, dab, **kwargs)`: returns an igraph VertexClustering object representing the clustered Dab

### ClusterMetric
*Properties*

   - `name`: a printable name of the metric 

*Public Methods*

   - `__init__(self, **kwargs)`: initialize the metric with optional parameters
   - `score(self, clustering, **kwargs)`: returns a value (usually a floating point number) that scores the given clustering

#### TestGenerator
*Public Methods*

   - `__init__(self, **kwargs)`: initialize the test generator with optional parameters
   - `generateTest(self, **kwargs)`: returns a two-element tuple -- first, a Dab to cluster and second,
                                    an optional dictionary of kwargs to be passed to a later ClusterMetric


In addition, there is one more wrapper class, that is used to more smoothly generate tests:
   - IndependentVar: defines an idependent variable in an experiment, 
                      along with the values and printed names for graphing

#### IndependentVar
*Properties*

   - `name`: a printable name for the variable
   - `values`: a list of objects that represent the values of the variable. The name property for each object
               is set to a custom label from `value_labels` if provided.

*Public Methods*

   - `__init__(self, name, values, value_labels=None)`: sets the properties based on the given parameters.
                If value_labels is not provided, then it will attempt to read the ".name" property from
                each of the objects in the *values* list. If this fails an exception will be thrown

*Example Usage*

To run a test seeing how an algorithm performs on clustering GO terms of different sizes:
```python
sizes = [10, 50, 100, 150]
generators = [NonOverlappingGOTerms(dab, go_terms, term_size=(k,k*2)) for k in sizes]
generator_var = IndependentVar("Term Size", generators, sizes)
```