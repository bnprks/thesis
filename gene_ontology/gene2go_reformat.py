# Filters a gene2go file (available at ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/) and
# outputs a .gmt file with the gene sets for humans
# Usage: gene2go_reformat.py output.gmt

# Note: Removes annotations supported solely by IEA evidence codes

import pandas as pd
import sys

def main():
    if len(sys.argv) != 2:
        print "Usage: gene2go_reformat.py output.gmt"
    
    f = open(sys.argv[1], 'w')

    t = pd.read_table("ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2go.gz", compression="gzip")

    t.rename(columns={'#tax_id':'tax_id'}, inplace=True)
    t = t.query('tax_id == 9606 & Evidence != "IEA" & Category == "Component"' )
    for ((id, term, category), group) in t.groupby(['GO_ID', 'GO_term',  'Category']):
        genes = map(str, group['GeneID'].drop_duplicates().tolist())
        f.write("%s\t%s (%s)\t%s\n" % (id, term, category, "\t".join(genes)))
    

if __name__ == '__main__':
    main()