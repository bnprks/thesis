# What are the differences between my old Gene Ontology file and the new one???

import sys
sys.path.append('../lib')
from Gmt import read_gmt

a = read_gmt(open('gobp_human.closed.gmt'))
b = read_gmt(open('go_human_bp.gmt')) 

a_ids = set(a.keys())
b_ids = set(b.keys())

mismatched_sets = []
for id in (a_ids & b_ids):
    if set(a[id]) != set(b[id]):
        mismatched_sets.append(id)