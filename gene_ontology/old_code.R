setwd('~/Dropbox/s2017/IW/code/gene_ontology/')

library(dplyr)

ontology <- read.csv('goa_human.gaf', header=FALSE, sep="\t", skip = 34)
colnames(ontology) <- c("DB","DB_Object_ID","DB_Object_Symbol",
                        "Qualifier","GO_ID","DB_Reference","Evidence_Code",
                        "With_or_From",
                        "Aspect","DB_Object_Name","DB_Object_Synonym",
                        "DB_Object_Type","Taxon_and_Interacting_taxon",
                        "Date","Assigned_By","Annotation_Extension",
                        "Gene_Product_Form_ID")
raw_ontology <- ontology
ontology <- raw_ontology %>% filter(!grepl("NOT", genes$Qualifier, fixed=TRUE)) %>%
  filter(Evidence_Code != "IEA")

uniprot_ids <- read.csv('uniprot-database-human-geneid.tsv', sep="\t")

#Q: How many genes in the Ontology don't have a corresponding ID in uniprot_ids
#A: 509
ontology %>% anti_join(uniprot_ids, by=c("DB_Object_ID"="Entry")) %>% 
  distinct(DB_Object_Symbol) %>% nrow

#Q: How does this overlap with genes from the other ID-mapping file?
unmatched_uniprot <- ontology %>% anti_join(uniprot_ids, by=c("DB_Object_ID"="Entry")) %>% 
  distinct(DB_Object_ID)

unmatched_uniprot %>% semi_join(unmatched_IDs, by=c("DB_Object_ID" = "uniprot_id")) %>% View

unmatched_IDs %>% anti_join(unmatched_uniprot, by=c("uniprot_id"= "DB_Object_ID")) %>% View

unmatched_uniprot$DB_Object_ID
unmatched_IDs$uniprot_id












putative_matches <- data.frame(entrez_id=c(), name=c())
cat("Name Matches\n")
for (name in unmatched_names$name) {
  alias_rows <- which(grepl(paste0("(^|\\|)",name,"($|\\|)"), names$alias_symbol))
  prev_rows <- which(grepl(paste0("(^|\\|)",name,"($|\\|)"), names$prev_symbol))
  combined_rows <- union(alias_rows, prev_rows)
  if(length(combined_rows) == 0) {
    cat(name, length(combined_rows), "\n")
    next()
  } else if (length(combined_rows) > 1) {
    cat(name, length(combined_rows), "\n")
  }
  putative_matches <- rbind(putative_matches, 
                            data.frame(entrez_id=names[combined_rows,]$entrez_id, name=name))
}