# Analyzes a network and a list of gene sets. Finds the average weight of network connections 
# within a gene set, compared to the weight of connections
# Usage: geneset_network_connectivity.py genesets.gmt [networks.dab]...

import numpy as np

import sys
sys.path.append('../lib')
from Dab import Dab
from Gmt import read_gmt

def co_annotation_counts(dab, gene_sets):
    co_annotated_pairs = {}
    name_to_idx = {}

    for i in range(len(dab.genes)):
        name_to_idx[dab.genes[i]] = i

    def register_pair(i, j):
        if (i,j) in co_annotated_pairs:
            co_annotated_pairs[(i,j)] += 1
        else:
            co_annotated_pairs[(i,j)] = 1

    def process_set(s):
        ids = sorted(name_to_idx[g] for g in s if g in name_to_idx)
        for (i,j) in itertools.combinations(ids, 2):
            register_pair(i,j)

    for s in gene_sets:
        process_set(s)
    return co_annotated_pairs
    

def co_annotated_weights(dab, gene_set):
    co_annotated_pairs = set()
    for s in gene_set.values():
        for i in range(len(s)):
            for j in range(i+1, len(s)):
                co_annotated_pairs.add((min(s[i],s[j]), max(s[i],s[j])))
    
    name_to_idx = {}
    for i in range(len(dab.genes)):
        name_to_idx[dab.genes[i]] = i

    vals = []
    for (x,y) in co_annotated_pairs:
        if x in name_to_idx and y in name_to_idx:
            vals.append(dab.weights[name_to_idx[x],
                                    name_to_idx[y]])
    
    return np.array(vals)


def main():
    if len(sys.argv) < 3:
        print "Usage: %s genesets.gmt [networks.dab]..." % sys.argv[0]
        return

    gene_set = read_gmt(open(sys.argv[1]))

    print "File\tOverall mean\tCo-annotated mean"
    for fname in sys.argv[2:]:
        d = Dab(fname)
        average_weight = d.weights.mean()

        co_annotated_average_weight = co_annotated_weights(d, gene_set).mean()
        print fname, average_weight, co_annotated_average_weight

if __name__ == '__main__':
    main()