#Just prints out a list of gene ids one per line, for use with
# http://www.uniprot.org/uploadlists/ (GeneID -> UniProtKB)

import sys
sys.path.append('../lib')
from Dab import Dab

f = open('gene_ids.txt', 'w')
d = Dab('../data/unfilt-brain.dab')

for g in d.genes:
    f.write(g + "\n")

f.close()