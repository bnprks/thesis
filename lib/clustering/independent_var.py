class IndependentVar:
    def __init__(self, name, values, value_labels=None):
        self.name = name
        self.values = values
        if value_labels:
            assert len(values) == len(value_labels)
            for i in range(len(values)):
                values[i].name = value_labels[i]
    
    # Iterates through the values of an IndependentVar.
    # If passed an instance of IndependentVar, iterates through
    # the objects in the value array, yielding 2-length tuples
    # of shape (object, {'name': 'value_label'}.
    # For other objects, just yields the value with an empty dictionary
    # as the second tuple element.
    @staticmethod
    def iterate(var):
        if isinstance(var, IndependentVar):
            for v in var.values:
                yield (v, {var.name: v.name})
        else:
            yield (var, {})