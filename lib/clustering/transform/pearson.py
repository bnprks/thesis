from base import GraphTransform
import numpy as np
from ...Dab import Dab
"""
### Pearson Correlation

This is a pretty standard Pearson Correlation Coefficient, with the one twist that I've added some tricks to exclude self-edges from the correlation calculations (so A[i,i] does not end up getting counted in the correlation). Note that it returns (correlation + 1)/2 so that the range of output is in [0,1]
"""

class Pearson(GraphTransform):
    name = "Pearson"
        
    # Perform the transform on raw matrices, where
    # A is the adjacency matrix and A2 = A*A
    # Note: this implemtation is tear-inducingly memory inefficient, but it's what
    # is possible in python (main problem is inefficient loops)
    def transform_matrices(self, A, A2):
        n = len(A)
        #Return a matrix A where A[i][j] = sum(mat[i][:] - mat[i][j])
        #This is useful in calculating the right averages for pairwise correlations
        #while ignoring the lack of self-edges
        def rowsum(mat):
            return  mat.sum(axis=1).reshape((mat.shape[0], 1)) - mat
        
        B = rowsum(A)
        
        # cov[i,j] = covariance of rows A[i], A[j] 
        # after removing comparisons between A[i,i] and A[i,j], etc.
        cov = A2 - (B * B.T) / (n-2)   
        
        # dev[i,j] = standard deviation of row A[i], when the entry A[i,j] is 
        # removed from consideration
        dev = (rowsum(A**2) - (B**2) / (n-2))**.5
        
        # Shift the correlation values to be in the range (0,1)
        return (1 + cov / (dev * dev.T)) / 2
        
    def transform(self, dab, **kwargs):
        n = len(dab.genes)
        A = dab.weights
        A2 = np.dot(dab.weights, dab.weights.T)
        
        # Construct the Dab to return
        out = Dab()
        out.genes = list(dab.genes)
        out.weights = self.transform_matrices(A, A2)
        # Zero out the diagonal
        out.weights[range(n), range(n)] = 0
        return out
        
    # Use a simpler algorithm to get a value for positions i and j
    def test_position(self, dab, i, j):
        return (np.corrcoef(
            np.delete(dab.weights[i,:], [i,j]),
            np.delete(dab.weights[j,:], [i,j])
        )[1,0] + 1)/2
        