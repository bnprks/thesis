from scipy import stats
from base import GraphTransform
import numpy as np
from ...Dab import Dab

class Spearman(GraphTransform):
    name = "Spearman"
    
    def transform(self, dab, **kwargs):
        n = len(dab.genes)
        A = dab.weights
        
        # Construct the Dab to return
        out = Dab()
        out.genes = list(dab.genes)
        out.weights = (1 + spearman_rho_only(A))/2
        return out
        
    # Use a simpler algorithm to get a value for positions i and j
    def test_position(self, dab, i, j):
        n = len(dab.genes)
        old_diag = dab.weights[range(n), range(n)]
        dab.weights[range(n), range(n)] = 1
        rho, _ = stats.spearmanr(dab.weights[i], dab.weights[j])
        dab.weights[range(n), range(n)] = old_diag
        return (1 + rho)/2

## These are just modified versions of the builtin scipy functions to
## remove any calculation of significance for the correlations
def spearman_rho_only(a, axis=1):
    n = len(a)
    old_diag = a[range(n), range(n)]
    a[range(n), range(n)] = 1
    
    ar = np.apply_along_axis(rankdata, axis, a)
    
    rs = np.corrcoef(ar, None, rowvar=axis)
    a[range(n), range(n)] = old_diag
    return rs

def rankdata(a, method='average'):
    if method not in ('average', 'min', 'max', 'dense', 'ordinal'):
        raise ValueError('unknown method "{0}"'.format(method))

    arr = np.ravel(np.asarray(a))
    algo = 'mergesort' if method == 'ordinal' else 'quicksort'
    sorter = np.argsort(arr, kind=algo)

    inv = np.empty(sorter.size, dtype=np.intp)
    inv[sorter] = np.arange(sorter.size, dtype=np.intp)

    if method == 'ordinal':
        return inv + 1

    arr = arr[sorter]
    obs = np.r_[True, arr[1:] != arr[:-1]]
    dense = obs.cumsum()[inv]

    if method == 'dense':
        return dense

    # cumulative counts of each unique value
    count = np.r_[np.nonzero(obs)[0], len(obs)]

    if method == 'max':
        return count[dense]

    if method == 'min':
        return count[dense - 1] + 1

    # average method
    return .5 * (count[dense] + count[dense - 1] + 1)

