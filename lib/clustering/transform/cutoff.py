from base import GraphTransform, gen_random_dab
from ...Dab import Dab
import numpy as np
"""
### Cutoff

Keeps the top x% of edge weights, while setting the rest to 0. Percentage is given in range of 0 to 100
"""

class Cutoff(GraphTransform):
    def __init__(self, percentile, **kwargs):
        self.percentile = percentile
        self.name = "Cutoff(%r)" % (percentile)
    def transform(self, dab, **kwargs):
        n = len(dab.genes)
        cutoff = np.percentile(dab.weights, 100 - self.percentile)
        out = Dab()
        out.genes = list(dab.genes)
        out.weights = np.where(dab.weights >= cutoff, dab.weights, 0)
        # Zero-out the diagonal
        out.weights[range(n), range(n)] = 0
        return out

def test_cutoff(size, percentile):
    print "Testing top %f percent cutoff on size %d matrix" % (percentile, size)
    dab = gen_random_dab(size)
    print "Non-zero values as %%", np.sum(Cutoff(percentile).transform(dab).weights > 0)/(float(size)**2)