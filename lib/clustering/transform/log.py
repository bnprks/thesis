from base import GraphTransform
from ...Dab import Dab
import numpy as np
import math
"""
### Log transform

Transform each edge by taking -log(1-weight). Note that this only works correctly on 
graphs with weights in the range [0,1), but it will return a graph with weights in the range [0, $\infty$)
"""

class Log(GraphTransform):
    name = "Log"
    def transform(self, dab, **kwargs):
        n = len(dab.genes)
        # Construct the Dab to return
        out = Dab()
        out.genes = list(dab.genes)
        out.weights = -np.log(1 - dab.weights)
        # Zero-out the diagonal
        out.weights[range(n), range(n)] = 0
        return out
    def test_position(self, dab, i, j):
        return -math.log(1 - dab.weights[i,j])