from base import GraphTransform
import numpy as np
from ...Dab import Dab

class Cosine(GraphTransform):
    name = "Cosine"
        
    # Perform the transform on raw matrices, where
    # A is the adjacency matrix and A2 = A*A
    # Note: this implemtation is tear-inducingly memory inefficient, but it's what
    # is possible in python (main problem is inefficient loops)
    def transform_matrices(self, A, A2):
        n = len(A)
        #Return a matrix A where A[i][j] = sum(mat[i][:] - mat[i][j])
        #This is useful in calculating the right averages for pairwise correlations
        #while ignoring the lack of self-edges
        def rowsum(mat):
            return  mat.sum(axis=1).reshape((mat.shape[0], 1)) - mat
        
        denom = rowsum(A**2)**.5
        # Shift the cosine values to be in the range (0, 1)
        return (1 + A2 / (denom * denom.T)) / 2
        
    def transform(self, dab, **kwargs):
        n = len(dab.genes)
        A = dab.weights
        A2 = np.dot(dab.weights, dab.weights.T)
        
        # Construct the Dab to return
        out = Dab()
        out.genes = list(dab.genes)
        out.weights = self.transform_matrices(A, A2)
        # Zero out the diagonal
        out.weights[range(n), range(n)] = 0
        return out
        
    # Use a simpler algorithm to get a value for positions i and j
    def test_position(self, dab, i, j):
        x = np.delete(dab.weights[i,:], [i,j])
        y = np.delete(dab.weights[j,:], [i,j])
        cos = np.dot(x,y)/(np.sqrt(np.dot(x,x)) * np.sqrt(np.dot(y,y)))
        return (1 + cos) / 2
        