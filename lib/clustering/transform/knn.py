from base import GraphTransform
from ...Dab import Dab
import numpy as np
"""
### KNN Transform

The K-Nearest-Neighbors transform for genes i and j is equal to the size of the intersection of each gene's k nearest neighbors, divided by k. Nearest neighbor is defined by highest network weight, and the whole transform is paramaterized on the variable "k"
"""

class KNN(GraphTransform):
    def __init__(self, k, **kwargs):
        self.name = "KNN(%d)" % k
        self.k = k
    def transform(self, dab, **kwargs):
        n = len(dab.genes)
        # Neighbors[i] holds the list of genes that have gene i as a k-nearest-neighbor 
        neighbors = [[] for i in range(n)]
        for i in range(n):
            knn = np.argpartition(dab.weights[i], -self.k)[-self.k:]
            for j in knn:
                neighbors[j].append(i)
        # Construct the Dab to return
        out = Dab()
        out.genes = list(dab.genes)
        out.weights = np.zeros((n,n), dtype=np.float64)
        
        for i in range(n):
            out.weights[np.ix_(neighbors[i], neighbors[i])] += 1.0/self.k
        # Zero-out the diagonal
        out.weights[range(n), range(n)] = 0
        return out
    
    # Use a simpler algorithm to get a value for positions i and j
    def test_position(self, dab, i, j):
        x = dab.weights[i]
        y = dab.weights[j]
        #Test that the partition to get k largest works
        knn_x = np.argpartition(x, -self.k)[-self.k:]
        knn_y = np.argpartition(y, -self.k)[-self.k:]
        knn_set_x = set(knn_x)
        knn_set_y = set(knn_y)
        m_x = min(x[knn_x])
        m_y = min(y[knn_y])
        for i in range(len(x)):
            if i not in knn_set_x:
                assert x[i] <= m_x
            if i not in knn_set_y:
                assert y[i] <= m_y
        return len(knn_set_x & knn_set_y)/float(self.k)