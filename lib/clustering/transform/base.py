import numpy as np
"""
### GraphTransform
*Properties*

   - name: a printable name of the graph transform 

*Public Methods*

   - `__init__(self, **kwargs)`: initialize the transformer with optional parameters
   - `transform(self, dab, **kwargs)`: returns a new Dab object formed by applying the transform to its argument.
      Weights should range from 0 to 1 in the transformed Dab, with 1 corresponding to strongly connected genes and
      0 corresponding to unconnected genes
"""

# Transform a graph adjacency matrix as a preprocessing step to clustering
class GraphTransform(object):
    name = 'None'

    def __init__(self, **kwargs):
        pass

    # Take in a Dab object, and return a Dab object with transformed 
    # weights
    def transform(self, dab, **kwargs):
        return dab


# Testing facilities for classes that implement a GraphTransform
def test_correctness(transformer, d, test_count=1000):
    out = transformer.transform(d)
    max_deviation = 0.0
    total_deviation = 0
    max_idxs = (-1,-1)
    for i in range(test_count):
        x, y = np.random.randint(len(d.genes), size=2)
        while x == y:
            x, y = np.random.randint(len(d.genes), size=2)
        dev = abs(transformer.test_position(d, x, y) - out.weights[x,y])
        total_deviation += dev
        if dev > max_deviation:
            max_deviation = dev
            max_idxs = (x, y)
    print "Average deviation: %f\tMax deviation: %f at (%d,%d)" % \
            (total_deviation/float(test_count), max_deviation, max_idxs[0], max_idxs[1])

def gen_random_dab(n):
    d = Dab()
    d.genes = [str(i) for i in range(n)]
    d.weights = np.random.random((n,n))
    d.weights[range(n), range(n)] = 0
    for i in range(1, n):
        d.weights[i:,i-1] = d.weights[i-1,i:]
    return d