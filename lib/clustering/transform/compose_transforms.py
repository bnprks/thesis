from base import GraphTransform
from ...Dab import Dab
import numpy as np
"""
### Compose Transform

Apply a list of multiple transforms in order on an input Dab
"""

class ComposeTransforms(GraphTransform):
    def __init__(self, transforms, **kwargs):
        self.transforms = transforms
        for t in transforms:
            assert isinstance(t, GraphTransform)
        self.name = " > ".join([t.name for t in transforms])

    def transform(self, dab, **kwargs):
        out = dab
        for t in self.transforms:
            out = t.transform(out)
        return out