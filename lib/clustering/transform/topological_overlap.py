from base import GraphTransform
from ...Dab import Dab
import numpy as np
"""
### Topological Overlap

This implements Topological Overlap as defined by Steve Horvath. 
Specifically this is GTOM1, linked here: https://labs.genetics.ucla.edu/horvath/GTOM/old/GTOM_tech_report.pdf
"""

class TopoOverlap(GraphTransform):
    name = "Topological Overlap"
        
    # Perform the transform on raw matrices, where
    # A is the adjacency matrix and A2 = A*A
    def transform_matrices(self, A, A2):
        n = len(A)
        num = A2 + A
        # Zero-out the diagonal of the matrix
        num[range(n), range(n)] = 0
        deg = np.sum(A, axis=1) #This appears to give better performance than axis=0
        d1 = np.reshape(deg, (n,1))
        d2 = np.reshape(deg, (1,n))
        denom = np.minimum(d1, d2) + 1 - A
        return num / denom
        
    def transform(self, dab, **kwargs):
        n = len(dab.genes)
        A = dab.weights
        A2 = np.dot(dab.weights, dab.weights.T)
        
        # Construct the Dab to return
        out = Dab()
        out.genes = list(dab.genes)
        out.weights = self.transform_matrices(A, A2)
        return out
        
    # Use a simpler algorithm to get a value for positions i and j
    def test_position(self, dab, i, j):
        n = len(dab.genes)
        num = 0.0
        for u in range(n):
            if u == i or u == j:
                continue
            num += dab.weights[i, u] * dab.weights[j, u]
        num += dab.weights[i, j]
        denom = min(sum(dab.weights[i]), sum(dab.weights[j])) + 1 - dab.weights[i,j]
        return num/denom
        