import igraph
# Evaluate a given clustering compared to a gold-standard clustering
class ClusterMetric(object):
    name = 'None'
    def __init__(self, **kwargs):
        pass

    # Take in two VertexClustering objects and return a floating point number
    def score(self, clustering, **kwargs):
        return 0

class Modularity(ClusterMetric):
    name = 'Modularity'
    def score(self, clustering, **kwargs):
        return clustering.modularity

class ClusterCount(ClusterMetric):
    name = 'Number of Clusters'
    def score(self, clustering, **kwargs):
        return len(clustering)

class ClusterSizes(ClusterMetric):
    name = 'Cluster Sizes'
    def score(self, clustering, **kwargs):
        sizes = map(str, sorted(clustering.sizes()))
        return ','.join(sizes)

class GoldStandardMetric(ClusterMetric):
    def _get_gold_standard(self, clustering, original_genesets):
        
        def get_gene_cluster(gene):
            for i, t in enumerate(original_genesets):
                if gene in original_genesets[t]:
                    return i

        g = clustering.graph
        gold_standard = igraph.VertexClustering(
            graph=g, 
            membership=[get_gene_cluster(gene) for gene in g.vs['name']]
        )

        return gold_standard

class AdjustedRand(GoldStandardMetric):
    name = 'Adjusted Rand Index'
    def score(self, clustering, original_genesets, **kwargs):
        gold_standard = self._get_gold_standard(clustering, original_genesets)
        return gold_standard.compare_to(clustering, method="adjusted_rand")

class AnnotationCorrespondence(GoldStandardMetric):
    name = 'Annotation Correspondence'
    def score(self, clustering, original_genesets, **kwargs):
        go_clustering = self._get_gold_standard(clustering, original_genesets)
        assert go_clustering.n == clustering.n
        num = 0.0
        for c in clustering:
            for t in go_clustering:
                num += len(set(c) & set(t))**2/len(c)
        return num/go_clustering.n
