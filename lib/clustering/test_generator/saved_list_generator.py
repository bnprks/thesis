# Write a test generator that can save and load a consistent set of genes
import json
from base import GeneListGenerator

class SavedListGenerator(GeneListGenerator):
    def __init__(self, dab, name='', **kwargs):
        self.current_index = -1
        self.list = []
        self.name = name
        self.dab = dab
    
    def generateGeneList(self, **kwargs):
        self.current_index += 1
        if self.current_index >= len(self.list):
            raise Exception("No more cases available from SavedListGenerator")
        return self.list[self.current_index]
    
    def addTest(self, gene_list, extra_info):
        self.list.append((gene_list, extra_info))
    
    def write(self, f):
        if type(f) is str:
            f = open(f, 'w')
        f.write(self.name + "\n")
        for gene_list, extra_info in self.list:
            f.write("\t".join(gene_list) + "\n")
            f.write(json.dumps(extra_info) + "\n")
    
    def load(self, f):
        if type(f) is str:
            f = open(f)
        lines = f.readlines()
        self.name = lines[0].strip()
        self.list = []
        for i in range(1, len(lines), 2):
            gene_list = lines[i].strip().split("\t")
            extra_info = json.loads(lines[i+1])
            self.list.append((gene_list, extra_info))
        return self

    def hasNext(self):
        return self.current_index + 1 < len(self.list)
 
