from ...Dab import Dab
"""
#### TestGenerator
*Public Methods*

   - `__init__(self, **kwargs)`: initialize the test generator with optional parameters
   - `generateTest(self, **kwargs)`: returns a two-element tuple -- first, a Dab to cluster and second,
                                    an optional dictionary of kwargs to be passed to a later ClusterMetric


In addition, there is one more wrapper class, that is used to more smoothly generate tests:
   - IndependentVar: defines an idependent variable in an experiment, 
                      along with the values and printed names for graphing
            
"""
class TestGenerator(object):
    name = 'None'
    def __init__(self, **kwargs):
        pass
    
    def generateTest(self, **kwargs):
        pass

class GeneListGenerator(TestGenerator):
    name = 'None'
    def __init__(self, dab, **kwargs):
        self.dab = dab
    
    def generateTest(self, **kwargs):
        gene_list, extra_info = self.generateGeneList(**kwargs)
        return self.dab.subset_genes(gene_list), extra_info

    def generateGeneList(self, **kwargs) :
        pass

