from base import GeneListGenerator
import numpy as np
"""
### NonOverlappingGOTerms

Generates sets of GO-terms with configurable cutoffs for maximum pairwise and global overlap
"""
class NonOverlappingGOTerms(GeneListGenerator):
    _defaults = {
        'term_size_range': (1,100000), 
        'total_size': 500,
        'attempts': 10,
    }

    def __init__(self, dab, all_terms, **kwargs):
        self.dab = dab
        # Filter the list of GO terms to only include genes that are in the Dab
        dab_genes = set(dab.genes)
        self.all_terms = {k:set(g for g in gene_list if g in dab_genes) for (k,gene_list) in all_terms.iteritems()}
        self._defaults.update(kwargs)
    
    # Given a list of term names, returns two sets (duplicates, all_genes)
    # duplicates is a set of duplicated genes; all_genes is the union of all the genes
    @staticmethod
    def get_duplicates(go_terms, term_names):
        duplicates = set()
        all_genes = set()
        for t_name in term_names:
            t = go_terms[t_name]
            duplicates |= all_genes & t
            all_genes |= t
        return duplicates, all_genes

    # Given a list of term names and initial conflicts, returns a list of terms that
    # can be removed to make the whole list satisfy the max_global_overlap requirement
    def get_conflicting_terms(self, go_terms, term_names, initial_conflicts, max_global_overlap):
        conflicts = set(initial_conflicts)
        no_removed_genes = False
        while not no_removed_genes:
            term_names = [t for t in term_names if t not in conflicts]
            duplicates, all_genes = self.get_duplicates(go_terms, term_names)
            no_removed_genes = True
            for t in np.random.permutation(term_names):
                if t in conflicts:
                    continue
                if len(go_terms[t] & duplicates) > len(go_terms[t]) * max_global_overlap:
                    conflicts.add(t)
                    no_removed_genes = False
                    break
        return list(conflicts)
    
    def get_nonoverlapping_terms(self, all_terms, total_size, term_size_range=(1,1000), 
                                 attempts=5, max_pairwise_overlap=0.1, max_global_overlap=0.33,
                                 verbose=False):

        min_size, max_size = term_size_range
        go_terms = {k: set(v) for (k,v) in all_terms.iteritems()
                    if min_size <= len(v) <= max_size}

        #Try the randomized process `attemts` times
        for i in range(attempts):
            chosen_terms = []
            for term in np.random.permutation(go_terms.keys()):
                duplicates, all_genes = self.get_duplicates(go_terms, chosen_terms)
                # Check if we've gotten enough genes
                if len(all_genes - duplicates) >= total_size:
                    return chosen_terms

                new_genes = go_terms[term]
                # Check if the new term violates global overlap threshold
                if len(new_genes & all_genes) > len(new_genes) * max_global_overlap: 
                    continue

                # List of terms that would need to be removed if new term is added
                conflicting_terms = [] 
                # Whether the current term has a pairwise conflict
                has_pairwise_conflict = False
                for t_name in chosen_terms:
                    pairwise_overlap = len(new_genes & go_terms[t_name])
                    if pairwise_overlap > len(go_terms[t_name]) * max_pairwise_overlap:
                        conflicting_terms.append(t_name)
                    if pairwise_overlap > len(new_genes) * max_pairwise_overlap:
                        has_pairwise_conflict = True
                        break
                if has_pairwise_conflict:
                    continue

                # Find how many genes would need to be removed to add this gene
                conflicting_terms = self.get_conflicting_terms(go_terms,
                                                          chosen_terms + [term], 
                                                          conflicting_terms,
                                                          max_global_overlap)
                genes_to_remove = sum(len(go_terms[t]) for t in conflicting_terms)
                genes_to_add = len(new_genes - all_genes)
                # With probability genes_to_add/(genes_to_add + genes_to_remove) add the new term
                if np.random.random() < float(genes_to_add)/(genes_to_add+genes_to_remove):
                    chosen_terms.append(term)
                    chosen_terms = [t for t in chosen_terms if t not in conflicting_terms]
            if verbose:   
                print "Failed test generation on iteration #%d: %d sets found" % ( i + 1,len(chosen_terms))

        raise Exception("Too many failed iterations while attempting to make non-overlapping terms (%d)" % attempts)

    def generateGeneList(self, **kwargs):
        params = self._defaults.copy()
        params.update(kwargs)
        term_names = self.get_nonoverlapping_terms(all_terms=self.all_terms,
                                                   **params)
        duplicates, all_genes = self.get_duplicates(self.all_terms, term_names)
        terms = {t: list(self.all_terms[t] - duplicates) for t in term_names}
        gene_list = list(all_genes - duplicates)
        return (gene_list, {'original_genesets': terms})
