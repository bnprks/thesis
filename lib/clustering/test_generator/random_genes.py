from base import GeneListGenerator
import numpy as np

# Generates a randomly selected set of N genes
class RandomSet(GeneListGenerator):
    name = 'Random'
    def __init__(self, dab, n, **kwargs):
        self.n = n
        self.dab = dab
    
    def generateGeneList(self, **kwargs):
        return np.random.choice(self.dab.genes, size=self.n, replace=False), {}