import igraph
from base import GraphCluster


# Make clustering algorithms for the built-in igraph clustering algorithms
class Louvain(GraphCluster):
    name = 'Louvain'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_multilevel(weights='weight')

class FastGreedy(GraphCluster):
    name = 'Greedy Modularity'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_fastgreedy(weights='weight').as_clustering()

class Infomap(GraphCluster):
    name = 'Infomap'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_infomap(edge_weights='weight')

class LeadingEigenvector(GraphCluster):
    name = 'Leading Eigenvector'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_leading_eigenvector(weights='weight')

class LabelPropagation(GraphCluster):
    name = 'Label Propagation'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_label_propagation(weights='weight')

class EdgeBetweenness(GraphCluster):
    name = 'Edge Betweenness'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_edge_betweenness(weights='weight').as_clustering()

class Spinglass(GraphCluster):
    name = 'Spinglass'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_spinglass(weights='weight')

class Walktrap(GraphCluster):
    name = 'Walkttrap'
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        return graph.community_walktrap(weights='weight').as_clustering()