import igraph
"""
### GraphCluster
*Properties*

   - `name`: a printable name of the clustering algorithm 

*Public Methods*

   - `__init__(self, **kwargs)`: initialize the clustering algorithm with optional parameters
   - `cluster(self, dab, **kwargs)`: returns an igraph VertexClustering object representing the clustered Dab

"""
# Cluster a graph
class GraphCluster(object):
    name = 'None'

    def __init__(self, **kwargs):
        pass
    
    # Take in a Dab object, and
    # return a VertexClustering object
    def cluster(self, dab, **kwargs):
        graph = self.dab_to_graph(dab)
        # Just put all the nodes into one cluster for the default implementation
        clust = igraph.VertexClustering(graph, membership=[0]*graph.vcount())
        return clust
    
    # Convert a Dab into a weighted igraph
    @staticmethod
    def dab_to_graph(dab):
        graph = igraph.Graph.Weighted_Adjacency(dab.weights.tolist(), mode=igraph.ADJ_UPPER, loops=False)
        graph.vs['name'] = dab.genes
        return graph
