from base import GraphCluster
from igraph_clustering import Louvain, FastGreedy, Infomap, LeadingEigenvector, LabelPropagation, EdgeBetweenness, Spinglass, Walktrap