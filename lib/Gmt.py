
def read_gmt(f):
    if type(f) is str:
        f = open(f)
    sets = {}
    for line in f:
        l = line.strip().split('\t')
        sets[l[0]] = l[2:]
    return sets