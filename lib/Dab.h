//Provides basic functions to read/write Dab files in C++
#include <blaze/Math.h>
#include <string>
#include <stdio.h>

using namespace blaze;
using namespace std;

typedef DynamicMatrix<double, rowMajor> MatrixType;

struct Network {
  MatrixType weights;
  vector<string> genes;
};

//Returns a network struct with the data from file f
Network readDab(FILE* f) {
  Network net;
  int N;
  //Read in # of genes
  fread((void *) &N, sizeof(N), 1, f);
  //Read in gene names
  net.genes = vector<string>(N);
  for (int i = 0; i < N; i++) {
    char c;
    //No, not a mistake! The file format really has null bytes
    //interleaved in the strings, so we need the extra seeks
    fseek(f, 1, SEEK_CUR);
    fread(&c, 1, 1, f);
    while(c != '\0') {
      net.genes[i] += c;
      fseek(f, 1, SEEK_CUR);
      fread(&c, 1, 1, f);
    }
  }
  //Read in gene weights
  float rowbuf[N];
  net.weights = MatrixType(N, N);
  for (int i = 0; i < (N-1); i++) {
    fread(rowbuf, sizeof(float), N-i-1, f);
    for(int j = 0; j < N - i - 1; j++) {
      net.weights(i, i+1+j) = rowbuf[j];
      net.weights(i+1+j, i) = rowbuf[j];
    }
  }
  return net;
}

//Writes out net to file f
void writeDab(Network &net, FILE* f) {
  int N = net.genes.size();
  //Write number of genes
  fwrite((void *) &N, sizeof(N), 1, f);
  //Write gene names
  char nullbyte = '\0';
  for (int i = 0; i < N; i++) {
    //Copy string characters with mysterious null byte interludes
    for(char& c : net.genes[i]) {
      fwrite((void *) &nullbyte, 1, 1, f);
      fwrite((void *) &c, 1, 1, f);
    }
    //Write null byte
    fwrite((void *) &nullbyte, 1, 1, f);
    fwrite((void *) &nullbyte, 1, 1, f);
  }
  //Write gene weights
  //float weight;
  DynamicMatrix<float, rowMajor> writeCopy = net.weights;
  for(int i = 0; i < N; i++) {
    fwrite((void *) (writeCopy.data(i) + i+1), sizeof(float), N-i-1, f);
    //Writing 1 by 1 is pretty slow
    /*for (int j = i+1; j < N; j++) {
      weight = net.weights(i,j);
      fwrite((void *) &weight, sizeof(float), 1, f);
    }*/
  }
  fflush(f);
}