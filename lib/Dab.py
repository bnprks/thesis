import sys
import numpy as np
import resource
import struct
import scipy.linalg

from numpy.random import random
import pandas as pd

class Dab:
    #For now, constructor is the same as read
    def __init__(self, f=None):
        if f:
            self.read(f)
        else:
            self.genes=[]
            self.weights=np.array([])

    #Pass in a file name or an open file handle
    def read(self, f):
        #Read a gene name from an open file handle
        def read_gene_name(f):
            buf = bytearray()
            while True:
                assert f.read(1) == '\0'
                b = f.read(1)
                if b is None or b == '\0':
                    return str(buf)
                else:
                    buf.append(b)

        #Reshape a vector of floats representing an upper-triangular
        #matrix into an NXN symmetrical matrix
        def reshape_to_symmetrical(l, N):
            m = np.zeros((N,N), dtype=np.float64)
            curidx = 0
            for i in xrange(N-1):
                m[i, i+1:N] = l[curidx:(curidx+N-i-1)]
                m[i+1:N, i] = l[curidx:(curidx+N-i-1)]
                curidx += N-i-1
            return m

        if isinstance(f, str):
            f = open(f)
        (ngenes,) = struct.unpack("i", f.read(4))
        self.genes = [None]*ngenes
        for i in xrange(ngenes):
            self.genes[i] = read_gene_name(f)
        flat_weights = np.fromfile(f, dtype=np.float32, count=(ngenes*(ngenes-1))/2)
        self.weights = reshape_to_symmetrical(flat_weights, ngenes)

    def write(self, f):
        if isinstance(f, str):
            f = open(f, 'w')
        #Write number of genes
        f.write(struct.pack("i", len(self.genes)))
        #Write gene names
        def write_gene_name(name, f):
            for c in name:
                f.write('\0')
                f.write(c)
            f.write('\0\0')
        for gene in self.genes:
            write_gene_name(gene, f)
        #Write weights
        float32_weights = np.array(self.weights, dtype=np.float32)
        for i in xrange(len(self.genes) - 1):
            f.write(float32_weights[i, i+1:].tobytes())
        f.flush()
    
    def subset_genes(self, gene_list):
        if not hasattr(self, 'gene_to_idx'):
            self.gene_to_idx = {self.genes[i]:i for i in range(len(self.genes))}
        
        ret = Dab()
        try:
            gene_idx = [self.gene_to_idx[g] for g in gene_list]
        except:
            raise ValueError("given gene_list contains genes not in the original Dab")
        ret.weights = self.weights[np.ix_(gene_idx, gene_idx)]
        ret.genes = gene_list
        return ret

def make_random_Dab(N):
    d = Dab()
    d.genes = map(str, range(N))
    d.weights = np.array(random((N,N)), dtype=np.float32)
    for i in xrange(N):
        d.weights[i,i+1:] = d.weights[i+1:,i]
    for i in xrange(N):
        d.weights[i][i] = 0
    return d


def get_linear_weights(dab):
    N = len(dab.genes)
    lin = np.zeros((N*(N-1))/2, dtype=np.float64)
    cur_idx = 0
    for i in range(N-1):
        lin[cur_idx:cur_idx+N-i-1] = dab.weights[i, i+1:N]
        cur_idx += N-i-1
    return lin