#A python implementation of matrix correlation on a dab file, assuming
#NaN's for all self-connections. Slow, but trustworthy
#Usage: correlate.py input.dab output.dab

import numpy as np
import pandas as pd

import sys
sys.path.append('../lib')
from Dab import Dab

def pandas_corr(d):
    df = pd.DataFrame(d.weights)
    for i in xrange(len(d.genes)):
        df[i][i] = np.nan
    ret = df.corr()
    return ret.values

def print_square_matrix(m):
    N = len(m)
    for i in range(N):
        for j in range(N):
            print "%.10f\t" % m[i][j],
        print

def main():
    if len(sys.argv) != 3:
        print "Usage: %s input.dab output.dab" % sys.argv[0]
        return
    d = Dab(sys.argv[1])
    d.weights = pandas_corr(d)
    d.write(sys.argv[2])
    print_square_matrix(d.weights[0:5,0:5])

if __name__ == '__main__':
    main()