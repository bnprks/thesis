#Compares two dab files for rough equivalence (extremely close floating point values)
#Usage: dabdiff.py file1.dab file2.dab


import numpy as np

import sys
sys.path.append('../lib')
from Dab import Dab


def main():
    if len(sys.argv) != 3:
        print "Usage: %s file1.dab file2.dab" % sys.argv[0]
        return
    d1 = Dab(sys.argv[1])
    d2 = Dab(sys.argv[2])
    #Check gene lists
    if d1.genes != d2.genes:
        print "Gene lists differ"
        return
    
    #Check all close
    are_close =  np.allclose(d1.weights, d2.weights, atol=1e-6, rtol=0)
    print "All entries differ by less than 10**-6: %s" % are_close

    #Check max difference
    diff = np.absolute(d1.weights - d2.weights)
    print "Max difference: %e" % diff.max()

    #Check percent difference
    pdiff = diff / d1.weights
    print "Max difference as percent: %e" % (np.nanmax(pdiff) * 100)

if __name__ == '__main__':
    main()