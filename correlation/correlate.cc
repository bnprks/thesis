#include "../lib/Dab.h"
#include <iostream>
#include <ctime>
#include <cblas.h>
#include <stdio.h>

//Performs the operation C = A*A' using the cblas_dsyrk function call
//Note that C is only returned with the upper triangle filled in
void blas_dsyrk(MatrixType &A, MatrixType &C) {
  cblas_dsyrk(CblasRowMajor, CblasUpper, CblasNoTrans,
    A.rows(), A.columns(), 1.0,
    A.data(), A.spacing(), 0.0,
    C.data(), C.spacing());
}

//Performs a correlation on the matrix m, putting the results into matrix out. 
void NetworkCorrelation(MatrixType &m, MatrixType &out) {
  int N = m.rows();
  //Calculate row-based operations
  auto sum = vector<double>(N, 0.0);
  auto sqsum = vector<double>(N, 0.0);
  for(int i = 0; i < N; i++) {
    for(int j = 0; j < N; j++) {
      sum[i] += m(i,j);
      sqsum[i] += m(i,j)*m(i,j);
    }
  }
  blas_dsyrk(m, out);
  //First do the non-diagonal entries
  for(int i = 0; i < N; i++) {
    for(int j = i+1; j < N; j++) {
      double numerator = (N-2)*out(i,j)-(sum[i]-m(i,j))*(sum[j]-m(i,j));
      double denominator = sqrt(((N-2)*(sqsum[i]-m(i,j)*m(i,j))-(sum[i]-m(i,j))*(sum[i]-m(i,j)))*
                                ((N-2)*(sqsum[j]-m(i,j)*m(i,j))-(sum[j]-m(i,j))*(sum[j]-m(i,j))));
      out(i,j) = numerator/denominator;
      out(j,i) = numerator/denominator;
    }
  }
  //Fix up symmetry
  for(int i = 0; i < N; i++) {
	  out(i,i) = 1.0;
  }
}

void printMatrix(MatrixType m) {
  for (unsigned int i = 0; i < m.rows(); i++) {
    for (unsigned int j = 0; j < m.columns(); j++) {
      printf("%.10f\t", m(i,j));
    }
    printf("\n");
  }
}

int main(int argc, char* argv[]) {
	if(argc != 3) {
    cout << "Usage: " << argv[0] << "input.dab output.dab" << endl;
    return 0;
  }
  auto infile = fopen(argv[1], "r");
  if (infile == NULL) {perror(argv[0]); return 1;}
	Network net = readDab(infile);
  
  Network out;
  out.genes = net.genes;
  out.weights = MatrixType(net.genes.size(),net.genes.size());
  auto start = clock();

  NetworkCorrelation(net.weights, out.weights);

  double duration = (clock() - start)/(double) CLOCKS_PER_SEC;
  cerr << "Time of operation: " << duration << " seconds" << endl;
  
  auto outfile = fopen(argv[2], "w");
  if (outfile == NULL) {perror(argv[0]); return 1;}
  writeDab(out, outfile);
  printMatrix(submatrix(out.weights,0,0,5,5));
}