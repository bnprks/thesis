#A python implementation of matrix correlation on a dab file, without nan adjustments
#JUST FOR PERFORMANCE COMPARISON -- DO NOT USE

import numpy as np

import sys
sys.path.append('../lib')
from Dab import Dab

d = Dab('../data/brain.dab')
d.weights = np.corrcoef(d.weights)

print d.weights[:5,:5]
