#include "../lib/Dab.h"
#include <iostream>
#include <ctime>
#include <cblas.h>
#include <stdio.h>

//Performs the operation C = A*A' using the cblas_dsyrk function call
//Note that C is only returned with the upper triangle filled in
void blas_dsyrk(MatrixType &A, MatrixType &C) {
  cblas_dsyrk(CblasRowMajor, CblasUpper, CblasNoTrans,
    A.rows(), A.columns(), 1.0,
    A.data(), A.spacing(), 0.0,
    C.data(), C.spacing());
}

//Performs a cosine distance calculation on the matrix m, putting the results into matrix out. 
void NetworkCosine(MatrixType &m, MatrixType &out) {
  int N = m.rows();
  //Calculate row-based operations

  blas_dsyrk(m, out);

  auto sqsum = vector<double>(N, 0.0);
  for(int i = 0; i < N; i++) {
    sqsum[i] = out(i,i);
  }
  //First do the non-diagonal entries
  for(int i = 0; i < N; i++) {
    for(int j = i+1; j < N; j++) {
      double result = out(i,j)/(sqrt(sqsum[i] - m(i,j)*m(i,j))*sqrt(sqsum[j] - m(i,j)*m(i,j)));
      out(i,j) = result;
      out(j,i) = result;
    }
  }
  //Fix up symmetry
  for(int i = 0; i < N; i++) {
	  out(i,i) = 1.0;
  }
}

void printMatrix(MatrixType m) {
  for (unsigned int i = 0; i < m.rows(); i++) {
    for (unsigned int j = 0; j < m.columns(); j++) {
      printf("%.10f\t", m(i,j));
    }
    printf("\n");
  }
}

int main(int argc, char* argv[]) {
	if(argc != 3) {
    cout << "Usage: " << argv[0] << "input.dab output.dab" << endl;
    return 0;
  }
  auto infile = fopen(argv[1], "r");
  if (infile == NULL) {perror(argv[0]); return 1;}
	Network net = readDab(infile);
  
  for(unsigned int i = 0; i < net.genes.size(); i++)
    net.weights(i,i) = 0;

  Network out;
  out.genes = net.genes;
  out.weights = MatrixType(net.genes.size(),net.genes.size());
  auto start = clock();

  NetworkCosine(net.weights, out.weights);

  double duration = (clock() - start)/(double) CLOCKS_PER_SEC;
  cerr << "Time of operation: " << duration << " seconds" << endl;
  
  auto outfile = fopen(argv[2], "w");
  if (outfile == NULL) {perror(argv[0]); return 1;}
  writeDab(out, outfile);
  printMatrix(submatrix(out.weights,0,0,5,5));
}